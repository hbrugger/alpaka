from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run

class SecondScreen(BaseView):
    def draw(self, ctx: Context) -> None:
        ctx.rgb(255, 255, 255).rectangle(-120, -120, 240, 240).fill()
        ctx.image("/flash/sys/apps/alpaka/alpaka1.png", -120, -80, 240, 172)

class AlpakaApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._frame = 0
        self._msSinceLastFrame = 0
        self._color = 0
        self._colorcount = 0
        self.Frequency_Hz = 3 # This is a constant. Use for configuration.

    def draw(self, ctx: Context) -> None:
        self._frame += 1
        self._frame %= 10

        ctx.rgb(255, 255, 255).rectangle(-120, -120, 240, 240).fill()
        ctx.image(f"/flash/sys/apps/alpaka/alpaka{self._frame}.png", -120, -80, 240, 172)
        ctx.image(f"/flash/sys/apps/alpaka/{self._color}.png", -120, -80, 240, 172)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        
        # self._msSinceLastFrame += delta_ms
        
        # It's time for a new frame!
        # if self._msSinceLastFrame >= 1000 / self.Frequency_Hz:
        #     self._frame += 1

        # Frame limit reached, going back to first one.
        # if self._frame >= 10:
        #     self._frame = 0
        #     self._msSinceLastFrame = 0

        if self.input.buttons.app.middle.pressed:
            self._colorcount += 1
            self._color = self._colorcount % 6

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(AlpakaApp(ApplicationContext()))
